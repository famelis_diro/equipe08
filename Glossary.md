Activit� physique - Activit� qui requiert un effort physique.

Sport - Activit� physique incluant des aspects du jeu, souvent accompagn� de l'aspect de comp�tition.

Centre sportif - Lieu o� on offre plusieurs activit�s physique et sport pour des membres.

Entra�nement - Suite d'activit�s physique � r�p�tition r�guli�re visant un but.

Machine d'entra�nement - Appareil avec lequel un membre peut pratiquer un activit� physique.

Entra�neur (personnel) : Professionel qui cr�e des entra�nements pour les membres du gym et qui leur donne des conseils.

Sant� physique : �tat de sant� du corps. Inclut autant les maladies que la condition du corps.

Membre : Individu qui est inscrit au gym comme utilisateur.

Abonnement : Contrat qui permet au membre d'utiliser le gym. Sous forme de paiement r�current.

S�ance : P�riode pr�d�fini o� un ou des membres participent � une activit� physique donn� par un professionel.