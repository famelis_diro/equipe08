import java.util.Scanner;

public class logiciel {
	
	static Scanner input = new Scanner(System.in);

	public class Membre {
		public void creationMembre() {
			String noMembre = "000000001";		//Devrait �tre un num�ro unique...
			String montantAdhesion = "75.00$"; //Montant fictif...
			System.out.println("Pr�nom : ");
			String prenom = input.nextLine();
			System.out.println("Nom : ");
			String nom = input.nextLine();
			System.out.println("Num�ro de t�l�phone : ");
			String noTel = input.nextLine();
			System.out.format("Veuillez payer le montant indiqu� � l'�cran : %s\n", montantAdhesion);
			//On offre les m�thodes de paiement et la transaction s'effectue...
			System.out.format("Nouveau membre cr�� avec succ�s! Voici son num�ro de membre : %s\n", noMembre);
			//On entre les informations dans le Centre de Donn�es...
		}
		public void paiementFrais(String noMembre) {
			Boolean noSuspendu = true;		//On suppose que le num�ro du client est suspendu...
			//On fait une recherche dans le Centre de Donn�es pour trouver le membre avec son num�ro...
			//Pour le prototype, le montant d� est pr�d�fini...
			System.out.println("Voici le montant que le client doit payer : 75.00$");
			//L'agent demande au client de payer, le client paie le montant...
			System.out.println("Le montant a �t� pay�, le num�ro est de nouveau valide.\n");
			noSuspendu = false;		//Le membre n'est plus suspendu...
		}
		public char numeroValide(String noMembre) {
			//On fait une recherche dans le Centre de Donn�es pour voir si le num�ro existe et s'il est valide...
			if(noMembre.equals("000000001")) {		//Pour le prototype, "000000001" existe et est valide...
				return '1';
			}
			else if(noMembre.equals("000000002")) {		//Pour le prototype, "000000002" est suspendu...
				return '0';
			}
			else return '2';				//Le num�ro n'existe pas, donc est invalide...
		}
		
	}

	public class Professionnel {
		public void creationProfessionnel() {
			String noProfessionnel = "100000001";		//Devrait �tre un num�ro unique...
			System.out.println("Pr�nom : ");
			String prenom = input.nextLine();
			System.out.println("Nom : ");
			String nom = input.nextLine();
			System.out.println("Num�ro de t�l�phone : ");
			String noTel = input.nextLine();
			System.out.format("Nouveau professionnel cr�� avec succ�s! "
					+ "Voici son num�ro de professionnel : %s\n", noProfessionnel);
			//On entre les informations dans le Centre de Donn�es...
		}
	}

	public class Service {
		public void creationService() {
			System.out.println("Veuillez entrer les informations suivantes :");
			//Date et heure actuelles (JJ-MM-AAAA HH:MM:SS)
			System.out.println("Date et heure actuelles (format : JJ-MM-AAAA HH:MM:SS) :");
			String dateActuelle = input.nextLine();
			//Date de d�but du service (JJ-MM-AAAA)
			System.out.println("Date de d�but du service (format : JJ-MM-AAAA) :");
			String dateDebut = input.nextLine();
			//Date de fin du service (JJ-MM-AAAA)
			System.out.println("Date de fin du service (format : JJ-MM-AAAA) :");
			String dateFin = input.nextLine();
			//Heure du service (HH:MM)
			System.out.println("Heure du service (format : HH:MM) :");
			String heureService = input.nextLine();
			//R�currence hebdomadaire du service (quels jours il est offert � la m�me heure)
			System.out.println("R�currence hebdomadaire du service :");
			String recurrenceHebdo = input.nextLine();
			//Capacit� maximale (maximum 30 inscriptions)
			System.out.println("Capacit� maximale (maximum 30 inscriptions) :");
			String capaciteMax = input.nextLine();
			//Num�ro du professionnel (9 chiffres)
			System.out.println("Num�ro du professionnel :");
			String noProfessionnel = input.nextLine();
			//Code du service (7 chiffres)
			System.out.println("Code du service :");
			String codeService = input.nextLine();
			//Frais du service (jusqu'� 100.00$)
			System.out.println("Frais du service (jusqu'� 100.00$) :");
			String fraisService = input.nextLine();
			//Commentaires (100 caract�res) (facultatif)
			System.out.println("Commentaires (facultatif, maximum 100 caract�res :");
			String commentaire = input.nextLine();
			System.out.println("Service cr�� avec succ�s!");
			//Les informations sont enregistr�es dans le Centre de Donn�es...
		}
		public void inscriptionService() {
			Boolean annuler = true;
			System.out.println("Veuillez indiquer la date que vous voulez vous inscrire (format : JJ-MM-AAAA) : ");
			String date = input.nextLine();
			this.consultationService(date);
			System.out.println("Veuillez choisir le service que vous voulez vous inscrire ou a pour annuler : ");
			//Pour le prototype, les montants sont pr�d�finis...
			while(annuler) {
				String choix = input.nextLine();
				switch(choix) {
					case "1":
						System.out.println("Voici le montant � payer : 50.00$\n");
						//Membre paie, le logiciel fait l'enregistrement...
						System.out.println("Le membre est inscrit � la s�ance!\n");
						return;
					case "2":
						System.out.println("Voici le montant � payer : 40.00$\n");
						//Membre paie, le logiciel fait l'enregistrement...
						System.out.println("Le membre est inscrit � la s�ance!\n");
						return;
					case "a":
						annuler = false;
						break;
					default:
						System.out.println("Choix non valide, veuillez r�essayer.\n");
						choix = input.nextLine();
						break;
				}
			}
		}
		public void confirmationService(String codeService) {
			Boolean noValide = true;	//On suppose que le num�ro du membre est valide pour le prototype...
			//Une recherche dans le R�pertoire des Services est effectu�e pour trouver le code...
			//Pour le prototype, un exemple est affich�...
			System.out.format("Code du service : %s\n"
					+ "Nom du service : S�ance de yoga\n"
					+ "Heure du service : HH:MM\n"
					+ "Num�ro du professionnel : 000000001\n", codeService);
			System.out.println("Voulez-vous confirmer (r) ou annuler (a) le processus?");
			while(true) {
				String choix = input.nextLine();
				switch(choix) {
					case "r":
						System.out.println("Veuillez entrer le num�ro du membre : ");
						String noMembre = input.nextLine();
						//V�rification dans le Centre de Donn�es si le num�ro du membre est valide...
						//Pour le prototype, on suppose qu'il est valide...
						if(noValide) {
							System.out.println("Valid�");
							/*Enregistrement dans le syst�me :
							 * -Date et heure actuelles (JJ-MM-AAAA HH:MM:SS) 
							 * -Num�ro du professionnel (9 chiffres)
							 * -Num�ro du membre (9 chiffres)
							 * -Code du service (7 chiffres)
							 * -Commentaires (100 caract�res) (facultatif).
							 */
							return;
						}
						else System.out.println("Num�ro invalide, acc�s refus�");
						return;
					case "a":
						System.out.println("Retour au menu principal."); 
						return;
					default:
						System.out.println("Choix non valide, veuillez r�essayer (r ou a) : ");
						break;
				}
				
			}
		}
		public void consultationService(String date) {
			//Une recherche dans le R�pertoire des Services selon la date est fait...
			//Pour le prototype, un exemple est affich�...
			System.out.println("Voici les services de cette journ�e :\n"
					+ "1. S�ance de yoga - HH:MM:SS\n"
					+ "2. S�ance de natation - HH:MM:SS\n");
		}
		public void consultationMembreService(String codeService) {
			//Une recherche dans le R�pertoire des Services selon le code de service est fait...
			//Pour le prototype, un exemple est affich�...
			System.out.println("Voici la liste des membres inscrits au service :\n"
					+ "John Doe\n"
					+ "Jane Doe\n");
		}
		public void annulerInscription(String codeService) {
			//Une recherche dans le R�pertoire des Services selon le code de service est fait...
			//Pour le prototype, un exemple est affich�...
			System.out.format("Voici le service recherch� :\n"
					+ "Code du service : %s\n" 
					+ "Nom du service : S�ance de yoga\n" 
					+ "Date et heure du service : JJ-MM-AAAA HH:MM\n"
					+ "Est-ce le bon service? ((o)ui ou (n)on) : ", codeService);
			while(true) {
				String choix = input.nextLine();
				switch(choix) {
					case "o":
						System.out.println("Veuillez entrer le num�ro du membre : ");
						String noMembre = input.nextLine();
						//Une recherche est fait pour trouver le membre dans la liste des participants...
						//Pour le prototype, on suppose que le client est inscrit � la s�ance...
						System.out.println("�tes-vous s�r de la d�sinscription du client? ((o)ui ou (n)on) : ");
						while(true) {
							choix = input.nextLine();
							switch(choix) {
								case "o":
									//L'agent rembourse le client et retire l'enregistrement du syst�me...
									return;
								case "n":
									System.out.println("Retour au menu principal.");
									return;
								default:
									System.out.println("Choix non valide, veuillez r�essayer (o ou n) : ");
									break;
							}
						}
					case "n":
						System.out.println("Retour au menu principal");
						return;
					default:
						System.out.println("Choix non valide, veuillez r�essayer (o ou n) : ");
						break;
				}
			}
		}
	}
	
	public static void rapportSynthese() {
		//Ex�cution du rapport synth�se et envoi du rapport au g�rant...
		System.out.println("Rapport de synth�se compl�t� et envoy�.");
	}

	public static void main(String[] args){
		logiciel utilisation = new logiciel();
		logiciel.Membre membre = utilisation.new Membre();
		logiciel.Professionnel professionnel = utilisation.new Professionnel();
		logiciel.Service service = utilisation.new Service();
		while(true) {
			System.out.println("Bonjour, voici les options � votre disposition :\n"
					+ "a. Donner acc�s � un membre au gym\n"
					+ "b. Inscrire un nouveau membre\n"
					+ "c. Payer les frais d'adh�sion mensuel\n"
					+ "d. Inscrire un nouveau professionnel\n"
					+ "e. Cr�er un nouveau service\n"
					+ "f. Consulter les services\n"
					+ "g. Consulter les membres d'un service\n"
					+ "h. Inscrire un membre � un service\n"
					+ "i. Confirmer la pr�sence � un service\n"
					+ "j. Annuler une inscription � une s�ance\n"
					+ "k. Ex�cuter le rapport de synth�se\n"
					+ "Veuillez entrer la lettre de votre choix ou q pour quitter : ");
			String choix = input.nextLine();
			switch(choix) {
			case "a":
				System.out.println("Veuillez entrer le num�ro du membre : ");
				String noMembre = input.nextLine();
				char noValide = membre.numeroValide(noMembre);
				if(noValide == '1') System.out.println("Valid�\n");
				else if(noValide == '0') {
					System.out.println("Num�ro suspendu, les frais d'adh�sion sont dus.");
					membre.paiementFrais(noMembre);
				}
				else if(noValide == '2') System.out.println("Num�ro invalide\n");
				break;
			case "b":
				membre.creationMembre();
				break;
			case "c":
				System.out.println("Veuillez entrer le num�ro du membre : ");
				noMembre = input.nextLine();
				membre.paiementFrais(noMembre);
				break;
			case "d":
				professionnel.creationProfessionnel();
				break;
			case "e":
				service.creationService();
				break;
			case "f":
				System.out.println("Veuillez indiquer la date que vous voulez consulter (format : JJ-MM-AAAA) : ");
				String date = input.nextLine();
				service.consultationService(date);
				break;
			case "g":
				System.out.println("Veuillez entrer le code de service que vous souhaiter voir : ");
				String codeService = input.nextLine();
				service.consultationMembreService(codeService);
				break;
			case "h":
				service.inscriptionService();
				break;
			case "i":
				System.out.println("Veuillez entrer le code de service : ");
				codeService = input.nextLine();
				service.confirmationService(codeService);
				break;
			case "j":
				System.out.println("Veuillez entrer le code de service : ");
				codeService = input.nextLine();
				service.annulerInscription(codeService);
				break;
			case "k":
				rapportSynthese();
				break;
			case "q":
				System.exit(0);
				break;
			default:
				System.out.println("Choix non valide, veuillez r�essayer.\n");
				break;
			}
		}
	}
	
}
